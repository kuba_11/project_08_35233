#include <iostream>
#include <fstream>
#include <string>
#include <ctime>
#include <limits>

using namespace std;

float cena_koszyka=0;
static int licznik=0;
static int licznik_napoje=0;

void Wyswietl_menu(fstream &plik)
{
    if(plik.is_open())
    {
        char wiersz[10000]; //maksymalnie 9999 znakow w wierszu
        while(plik.getline(wiersz,10000)) //dopoki jest co czytac
        {
            cout<<wiersz<<endl; //wypisz to co wczytales z pliku
            //lub wykonaj inna operacje
        }
    }
}

class cPizza
{
public:
    string nazwa;
    float cena_malej;
    float cena_sredniej;
    float cena_duzej;
    float cena_polowa_duzej;
    float cena_zestaw;
};

class cNapoj
{
public:
    string nazwa;
    float cena_napoju;
};

void koszyk(int liczba, fstream &paragon)
{
    licznik++;
    string nazwa_pizzy="";
    int cena_pizzy=0;
    cPizza p1,p2,p3,p4,p5,p6,p7,p8,p9,p10;
    p1.nazwa="MARGARITA";
    p1.cena_malej=18.00;
    p1.cena_sredniej=25.00;
    p1.cena_duzej=29.00;

    p2.nazwa="CHIPPOLA";
    p2.cena_malej=19.00;
    p2.cena_sredniej=25.00;
    p2.cena_duzej=31.00;

    p3.nazwa="SALAMI";
    p3.cena_malej=20.00;
    p3.cena_sredniej=26.00;
    p3.cena_duzej=33.00;

    p4.nazwa="PROSCIUTTO";
    p4.cena_malej=20.00;
    p4.cena_sredniej=26.00;
    p4.cena_duzej=33.00;

    p5.nazwa="FUNGI";
    p5.cena_malej=20.00;
    p5.cena_sredniej=26.00;
    p5.cena_duzej=33.00;

    p6.nazwa="PEPPERONI";
    p6.cena_malej=20.00;
    p6.cena_sredniej=26.00;
    p6.cena_duzej=33.00;

    p7.nazwa="RUSTICA";
    p7.cena_malej=21.00;
    p7.cena_sredniej=28.00;
    p7.cena_duzej=34.00;

    p8.nazwa="AMOROSA";
    p8.cena_malej=22.00;
    p8.cena_sredniej=29.00;
    p8.cena_duzej=34.00;

    p9.nazwa="ZESTAW RODZINNY";
    p9.cena_zestaw=90.00;

    p10.nazwa="ZESTAW STUDENTA";
    p10.cena_zestaw=40.00;

    cNapoj n1;
    n1.nazwa="COLA";
    n1.cena_napoju=0.00;

    cNapoj n2;
    n2.nazwa="SPRITE";
    n2.cena_napoju=10.00;

    switch( liczba )
    {
    case 1:
    {
        string rozmiar;
        cout << "Wpisz: duza, mala, srednia" << endl;
        cin >> rozmiar;
        if(rozmiar=="mala")
        {
            cena_koszyka+=p1.cena_malej;
            //WPISYWANIE NAZWY PIZZY I CENY DO PLIKU TXT I PRZY WYSTAWIANIU PARAGONU BEDZIE POBIERANE
            nazwa_pizzy=p1.nazwa;
            cena_pizzy=p1.cena_malej;
            paragon<<nazwa_pizzy<<" ";
            paragon<<cena_pizzy<<endl;

        }
        else if(rozmiar=="srednia")
        {
            cena_koszyka+=p1.cena_sredniej;
            nazwa_pizzy=p1.nazwa;
            cena_pizzy=p1.cena_sredniej;
            paragon<<nazwa_pizzy<<" ";
            paragon<<cena_pizzy<<endl;
        }
        else if(rozmiar=="duza")
        {
            cena_koszyka+=p1.cena_duzej;
            nazwa_pizzy=p1.nazwa;
            cena_pizzy=p1.cena_duzej;
            paragon<<nazwa_pizzy<<" ";
            paragon<<cena_pizzy<<endl;
        }
        else if(!(rozmiar=="mala" || rozmiar=="srednia" || rozmiar=="duza")) //zabezpieczenie
        {
            cout << "Podales zle dane. Wpisz jeszcze raz" << endl;
        }
        break;
    }
    case 2:
    {
        string rozmiar;
        cout << "Wpisz: duza, mala, srednia" << endl;
        cin >> rozmiar;
        if(rozmiar=="mala")
        {
            cena_koszyka+=p2.cena_malej;
            nazwa_pizzy=p2.nazwa;
            cena_pizzy=p2.cena_malej;
            paragon<<nazwa_pizzy<<" ";
            paragon<<cena_pizzy<<endl;

        }
        else if(rozmiar=="srednia")
        {
            cena_koszyka+=p2.cena_sredniej;
            nazwa_pizzy=p2.nazwa;
            cena_pizzy=p2.cena_sredniej;
            paragon<<nazwa_pizzy<<" ";
            paragon<<cena_pizzy<<endl;
        }
        else if(rozmiar=="duza")
        {
            cena_koszyka+=p2.cena_duzej;
            nazwa_pizzy=p2.nazwa;
            cena_pizzy=p2.cena_duzej;
            paragon<<nazwa_pizzy<<" ";
            paragon<<cena_pizzy<<endl;
        }
        else if(!(rozmiar=="mala" || rozmiar=="srednia" || rozmiar=="duza")) //zabezpieczenie
        {
            cout << "Podales zle dane. Wpisz jeszcze raz" << endl;
        }
        break;
    }
    case 3:
    {
        string rozmiar;
        cout << "Wpisz: duza, mala, srednia" << endl;
        cin >> rozmiar;
        if(rozmiar=="mala")
        {
            cena_koszyka+=p3.cena_malej;
            nazwa_pizzy=p3.nazwa;
            cena_pizzy=p3.cena_malej;
            paragon<<nazwa_pizzy<<" ";
            paragon<<cena_pizzy<<endl;

        }
        else if(rozmiar=="srednia")
        {
            cena_koszyka+=p3.cena_sredniej;
            nazwa_pizzy=p3.nazwa;
            cena_pizzy=p3.cena_sredniej;
            paragon<<nazwa_pizzy<<" ";
            paragon<<cena_pizzy<<endl;
        }
        else if(rozmiar=="duza")
        {
            cena_koszyka+=p3.cena_duzej;
            nazwa_pizzy=p3.nazwa;
            cena_pizzy=p3.cena_duzej;
            paragon<<nazwa_pizzy<<" ";
            paragon<<cena_pizzy<<endl;
        }
        else if(!(rozmiar=="mala" || rozmiar=="srednia" || rozmiar=="duza")) //zabezpieczenie
        {
            cout << "Podales zle dane. Wpisz jeszcze raz" << endl;
        }
        break;
    }
    case 4:
    {
        string rozmiar;
        cout << "Wpisz: duza, mala, srednia" << endl;
        cin >> rozmiar;
        if(rozmiar=="mala")
        {
            cena_koszyka+=p4.cena_malej;
            nazwa_pizzy=p4.nazwa;
            cena_pizzy=p4.cena_malej;
            paragon<<nazwa_pizzy<<" ";
            paragon<<cena_pizzy<<endl;

        }
        else if(rozmiar=="srednia")
        {
            cena_koszyka+=p4.cena_sredniej;
            nazwa_pizzy=p4.nazwa;
            cena_pizzy=p4.cena_sredniej;
            paragon<<nazwa_pizzy<<" ";
            paragon<<cena_pizzy<<endl;
        }
        else if(rozmiar=="duza")
        {
            cena_koszyka+=p4.cena_duzej;
            nazwa_pizzy=p4.nazwa;
            cena_pizzy=p4.cena_duzej;
            paragon<<nazwa_pizzy<<" ";
            paragon<<cena_pizzy<<endl;
        }
        else if(!(rozmiar=="mala" || rozmiar=="srednia" || rozmiar=="duza")) //zabezpieczenie
        {
            cout << "Podales zle dane. Wpisz jeszcze raz" << endl;
        }
        break;
    }
    case 5:
    {
        string rozmiar;
        cout << "Wpisz: duza, mala, srednia" << endl;
        cin >> rozmiar;
        if(rozmiar=="mala")
        {
            cena_koszyka+=p5.cena_malej;
            nazwa_pizzy=p5.nazwa;
            cena_pizzy=p5.cena_malej;
            paragon<<nazwa_pizzy<<" ";
            paragon<<cena_pizzy<<endl;

        }
        else if(rozmiar=="srednia")
        {
            cena_koszyka+=p5.cena_sredniej;
            nazwa_pizzy=p5.nazwa;
            cena_pizzy=p5.cena_sredniej;
            paragon<<nazwa_pizzy<<" ";
            paragon<<cena_pizzy<<endl;
        }
        else if(rozmiar=="duza")
        {
            cena_koszyka+=p5.cena_duzej;
            nazwa_pizzy=p5.nazwa;
            cena_pizzy=p5.cena_duzej;
            paragon<<nazwa_pizzy<<" ";
            paragon<<cena_pizzy<<endl;
        }
        else if(!(rozmiar=="mala" || rozmiar=="srednia" || rozmiar=="duza")) //zabezpieczenie
        {
            cout << "Podales zle dane. Wpisz jeszcze raz" << endl;
        }
        break;
    }
    case 6:
    {
        string rozmiar;
        cout << "Wpisz: duza, mala, srednia" << endl;
        cin >> rozmiar;
        if(rozmiar=="mala")
        {
            cena_koszyka+=p6.cena_malej;
            nazwa_pizzy=p6.nazwa;
            cena_pizzy=p6.cena_malej;
            paragon<<nazwa_pizzy<<" ";
            paragon<<cena_pizzy<<endl;

        }
        else if(rozmiar=="srednia")
        {
            cena_koszyka+=p6.cena_sredniej;
            nazwa_pizzy=p6.nazwa;
            cena_pizzy=p6.cena_sredniej;
            paragon<<nazwa_pizzy<<" ";
            paragon<<cena_pizzy<<endl;
        }
        else if(rozmiar=="duza")
        {
            cena_koszyka+=p6.cena_duzej;
            nazwa_pizzy=p6.nazwa;
            cena_pizzy=p6.cena_duzej;
            paragon<<nazwa_pizzy<<" ";
            paragon<<cena_pizzy<<endl;
        }
        else if(!(rozmiar=="mala" || rozmiar=="srednia" || rozmiar=="duza")) //zabezpieczenie
        {
            cout << "Podales zle dane. Wpisz jeszcze raz" << endl;
        }
        break;
    }
    case 7:
    {
        string rozmiar;
        cout << "Wpisz: duza, mala, srednia" << endl;
        cin >> rozmiar;
        if(rozmiar=="mala")
        {
            cena_koszyka+=p7.cena_malej;
            nazwa_pizzy=p7.nazwa;
            cena_pizzy=p7.cena_malej;
            paragon<<nazwa_pizzy<<" ";
            paragon<<cena_pizzy<<endl;

        }
        else if(rozmiar=="srednia")
        {
            cena_koszyka+=p7.cena_sredniej;
            nazwa_pizzy=p7.nazwa;
            cena_pizzy=p7.cena_sredniej;
            paragon<<nazwa_pizzy<<" ";
            paragon<<cena_pizzy<<endl;
        }
        else if(rozmiar=="duza")
        {
            cena_koszyka+=p7.cena_duzej;
            nazwa_pizzy=p7.nazwa;
            cena_pizzy=p7.cena_duzej;
            paragon<<nazwa_pizzy<<" ";
            paragon<<cena_pizzy<<endl;
        }
        else if(!(rozmiar=="mala" || rozmiar=="srednia" || rozmiar=="duza")) //zabezpieczenie
        {
            cout << "Podales zle dane. Wpisz jeszcze raz" << endl;
        }
        break;
    }
    case 8:
    {
        string rozmiar;
        cout << "Wpisz: duza, mala, srednia" << endl;
        cin >> rozmiar;
        if(rozmiar=="mala")
        {
            cena_koszyka+=p8.cena_malej;
            nazwa_pizzy=p8.nazwa;
            cena_pizzy=p8.cena_malej;
            paragon<<nazwa_pizzy<<" ";
            paragon<<cena_pizzy<<endl;

        }
        else if(rozmiar=="srednia")
        {
            cena_koszyka+=p8.cena_sredniej;
            nazwa_pizzy=p8.nazwa;
            cena_pizzy=p8.cena_sredniej;
            paragon<<nazwa_pizzy<<" ";
            paragon<<cena_pizzy<<endl;
        }
        else if(rozmiar=="duza")
        {
            cena_koszyka+=p8.cena_duzej;
            nazwa_pizzy=p8.nazwa;
            cena_pizzy=p8.cena_duzej;
            paragon<<nazwa_pizzy<<" ";
            paragon<<cena_pizzy<<endl;
        }
        else if(!(rozmiar=="mala" || rozmiar=="srednia" || rozmiar=="duza")) //zabezpieczenie
        {
            cout << "Podales zle dane. Wpisz jeszcze raz" << endl;
        }
        break;
    }
    case 9:
    {
        cena_koszyka+=p9.cena_zestaw;
        nazwa_pizzy=p9.nazwa;
        cena_pizzy=p9.cena_zestaw;
        paragon<<nazwa_pizzy<<" ";
        paragon<<cena_pizzy<<endl;
        break;
    }
    case 10:
    {
        cena_koszyka+=p10.cena_zestaw;
        nazwa_pizzy=p10.nazwa;
        cena_pizzy=p10.cena_zestaw;
        paragon<<nazwa_pizzy<<" ";
        paragon<<cena_pizzy<<endl;
        break;
    }
    case 11:
    {
        cena_koszyka+=n2.cena_napoju;
        nazwa_pizzy=n2.nazwa;
        cena_pizzy=n2.cena_napoju;
        paragon<<nazwa_pizzy<<" ";
        paragon<<cena_pizzy<<endl;
        licznik_napoje++;
        break;
    }
    }
    if(liczba==0)
    {
        licznik--;
    }

    if(licznik-licznik_napoje==2) //promocja napoj za darmo
    {
        cena_koszyka+=n1.cena_napoju;
        nazwa_pizzy=n1.nazwa;
        cena_pizzy=n1.cena_napoju;
        paragon<<nazwa_pizzy<<" ";
        paragon<<cena_pizzy<<endl;
        licznik++;
    }
}

void promka(int liczba, fstream &paragon)
{
    licznik++;
    string nazwa_pizzy="";
    int cena_pizzy=0;

    cPizza p1,p2,p3,p4,p5,p6,p7,p8;

    p1.nazwa="MARGARITA polowa";
    p1.cena_polowa_duzej=14.50;

    p2.nazwa="CHIPPOLA polowa";
    p2.cena_polowa_duzej=15.50;

    p3.nazwa="SALAMI polowa";
    p3.cena_polowa_duzej=16.50;

    p4.nazwa="PROSCIUTTO polowa";
    p4.cena_polowa_duzej=16.50;

    p5.nazwa="FUNGI polowa";
    p5.cena_polowa_duzej=16.50;

    p6.nazwa="PEPPERONI polowa";
    p6.cena_polowa_duzej=16.50;

    p7.nazwa="RUSTICA polowa";
    p7.cena_polowa_duzej=17.00;

    p8.nazwa="AMOROSA polowa";
    p8.cena_polowa_duzej=17.00;

    switch( liczba )
    {
    case 1:
    {
        cena_koszyka+=p1.cena_polowa_duzej;
        nazwa_pizzy=p1.nazwa;
        cena_pizzy=p1.cena_polowa_duzej;
        paragon<<nazwa_pizzy<<" ";
        paragon<<cena_pizzy<<endl;
        break;
    }
    case 2:
    {
        cena_koszyka+=p2.cena_polowa_duzej;
        nazwa_pizzy=p2.nazwa;
        cena_pizzy=p2.cena_polowa_duzej;
        paragon<<nazwa_pizzy<<" ";
        paragon<<cena_pizzy<<endl;
        break;
    }
    case 3:
    {
        cena_koszyka+=p3.cena_polowa_duzej;
        nazwa_pizzy=p3.nazwa;
        cena_pizzy=p3.cena_polowa_duzej;
        paragon<<nazwa_pizzy<<" ";
        paragon<<cena_pizzy<<endl;
        break;
    }
    case 4:
    {
        cena_koszyka+=p4.cena_polowa_duzej;
        nazwa_pizzy=p4.nazwa;
        cena_pizzy=p4.cena_polowa_duzej;
        paragon<<nazwa_pizzy<<" ";
        paragon<<cena_pizzy<<endl;
        break;
    }
    case 5:
    {
        cena_koszyka+=p5.cena_polowa_duzej;
        nazwa_pizzy=p5.nazwa;
        cena_pizzy=p5.cena_polowa_duzej;
        paragon<<nazwa_pizzy<<" ";
        paragon<<cena_pizzy<<endl;
        break;
    }
    case 6:
    {
        cena_koszyka+=p6.cena_polowa_duzej;
        nazwa_pizzy=p6.nazwa;
        cena_pizzy=p6.cena_polowa_duzej;
        paragon<<nazwa_pizzy<<" ";
        paragon<<cena_pizzy<<endl;
        break;
    }
    case 7:
    {
        cena_koszyka+=p7.cena_polowa_duzej;
        nazwa_pizzy=p7.nazwa;
        cena_pizzy=p7.cena_polowa_duzej;
        paragon<<nazwa_pizzy<<" ";
        paragon<<cena_pizzy<<endl;
        break;
    }
    case 8:
    {
        cena_koszyka+=p8.cena_polowa_duzej;
        nazwa_pizzy=p8.nazwa;
        cena_pizzy=p8.cena_polowa_duzej;
        paragon<<nazwa_pizzy<<" ";
        paragon<<cena_pizzy<<endl;
        break;
    }
    }

}

int main()
{
    //TUTAJ WYPISUJEMY MENU Z PLIKU menu.txt
    fstream menu;
    menu.open( "menu.txt", ios::in );
    if( menu.good() != true )
    {
        cout << "Nie mozna otworzyc pliku" << endl;
        menu.close();
        return 1;
    }
    Wyswietl_menu(menu);
    menu.close();

    cout << endl << "Podaj 50 jesli chcesz kupic pizze 50/50" <<endl;
    cout << "Podaj 0 jesli wybrales juz wszystkie produkty" <<endl<<endl;

    fstream paragon; //TWORZYMY PLIK PARAGON
    paragon.open( "paragon.txt", ios::out );
    if( paragon.good() != true )
    {
        cout << "Nie mozna otworzyc pliku" << endl;
        paragon.close();
        return 1;
    }

    //TUTAJ WYPISUJEMY PARAGON DO PLIKU
    string napis="PIZZERIA U ANDRZEJA";
    paragon<<napis<<endl;
    //WYPISYWANIE CZASU BEDZIE POTRZEBNE DO PARAGONU
    time_t czas = 0;
    time( & czas );
    paragon<<ctime(&czas);//<<endl;
    paragon<< "__________________________"<< endl;

    for(;;)
    {
        int numer_pizzy;
        cout << "Wpisz numer produktu! (0 wystawia paragon)" << endl;

        while(!(cin>>numer_pizzy)) //zabezpieczenie przed wpisaniem liter
        {
            cout<<"Podales niepoprawny numer pizzy"<<endl;
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(),'\n');
            cout << "Wpisz poprawny numer pizzy!" << endl;
        }

        if(cena_koszyka==0 && numer_pizzy==0)
        {
            koszyk(numer_pizzy,paragon);
        }
        else if(numer_pizzy==0) //JESLI WPISZEMY 0 to BEDZIE wystawialo paragon
        {
            paragon<< "__________________________"<< endl;
            paragon<<"Liczba produktow: "<<licznik<<endl;
            break;
        }
        else if(numer_pizzy==50) //PROMOCJA 50/50
        {
            //pizza pierwsza
            int pizza_pierwsza;
            cout << "Wpisz numer pierwszej pizzy w promocji 50/50" << endl;

            while(!(cin>>pizza_pierwsza) || pizza_pierwsza<=0 || pizza_pierwsza>8) //zabezpieczenie przed wpisaniem liter
            {
                cout<<"Podales niepoprawny numer pizzy"<<endl;
                cin.clear();
                cin.ignore(numeric_limits<streamsize>::max(),'\n');
                cout << "Wpisz poprawny numer pizzy!" << endl;
            }

            promka(pizza_pierwsza,paragon);

            //pizza druga
            int pizza_druga;
            cout << "Wpisz numer drugiej pizzy w promocji 50/50" << endl;

            while(!(cin>>pizza_druga) || pizza_druga<=0 || pizza_druga>8) //zabezpieczenie przed wpisaniem liter
            {
                cout<<"Podales niepoprawny numer pizzy"<<endl;
                cin.clear();
                cin.ignore(numeric_limits<streamsize>::max(),'\n');
                cout << "Wpisz poprawny numer pizzy!" << endl;
            }

            promka(pizza_druga,paragon);
            licznik--;
        }
        else if(numer_pizzy>0 && numer_pizzy<=11)
        {
            koszyk(numer_pizzy,paragon);
            cout << endl;
        }
        else if(numer_pizzy>10) //JESLI W MENU BEDZIE 10 POZYCJI
        {
            cout << "Podales zly numer pizzy!" << endl;
        }
    }

    if(cena_koszyka>100) //promocja -20%
    {
        paragon<<"ZNIZKA 20 PROCENT!!"<<endl;
        cena_koszyka=cena_koszyka*0.8;
    }

    string cena="SUMA PLN: ";
    paragon<<cena;
    paragon<<cena_koszyka<<endl;
    paragon.close(); //koniec zapisywania do paragonu

    //WYPISUJEMY PARAGON Z PLIKU
    cout << endl << "PARAGON: "<< endl;
    cout << endl << "__________________________"<< endl;

    fstream wynik;
    wynik.open( "paragon.txt", ios::in );
    if( wynik.good() != true )
    {
        cout << "Nie mozna otworzyc pliku" << endl;
        wynik.close();
        return 1;
    }
    if(wynik.is_open())
    {
        char wiersz[10000];//maksymalnie 9999 znakow w wierszu
        while(wynik.getline(wiersz,10000)) //dopoki jest co czytac
        {
            cout<<wiersz<<endl; //wypisz to co wczytales z pliku
            //lub wykonaj inna operacje
        }
    }
    wynik.close();
    cout << "__________________________"<< endl;


    return 0;
}
